package simulationClasses;

import java.util.Optional;

public class Cell {
    private final Board board;
    private final int x;
    private final int y;
    private final int state;

    public Cell(Board board, int x, int y, int state) {
        this.board = board;
        this.x = x;
        this.y = y;
        this.state = state;
    }

    public int countNumberOfAliveNeighbours() {
        int numberOfAliveNeighbours = 0;

        for (int i = -1; i <= 1; i++) {
            for (int j = -1; j <= 1; j++) {
                Optional<Cell> neighbourCell = board.getCell(x + i, y + j);
                if (neighbourCell.isPresent())
                    if (neighbourCell.get() != this && neighbourCell.get().isAlive())
                        numberOfAliveNeighbours ++;
            }
        }

        return numberOfAliveNeighbours;
    }

    public boolean isAlive() {
        return state == 1;
    }

    public int getState() {
        return state;
    }
}
