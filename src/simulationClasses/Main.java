package simulationClasses;

import sim.core.Manager;

import java.util.Optional;

public class Main {
//    N generacji (20<N<50);
    private static final double startTime = 0.0;
    private static final double timeStep = 1.0;
    private static final double endSimTime = 30.0;

    public static void main(String[] args) {
        Manager simMngr = Manager.getInstance(startTime, timeStep);
        simMngr.setEndSimTime(endSimTime);

        Board board = new Board(5,7);
        board.fillInRandomly();
        board.print();
        System.out.println();

        new TheGameOfLife(simMngr, board);
        simMngr.startSimulation();

    }
}
