package simulationClasses;

import sim.core.Manager;
import sim.core.SimStep;
import sim.monitors.MonitoredVar;

public class TheGameOfLife extends SimStep {
    private Board board;

    public TheGameOfLife(Manager mngr, Board board) {
        super(mngr);
        this.board = board;
    }

    @Override
    public void stateChange() {
        Board newBoard = new Board(this.board.getRows(), this.board.getColumns());
        newBoard.copyCellValues(this.board);
        Cell[][] cells = this.board.getCells();
        for (int i = 1; i < this.board.getRows()-1; i++) {
            for (int j = 1; j < this.board.getColumns()-1; j++) {
                int state = countNewState(cells[i][j]);
                newBoard.setCell(i,j,state);
            }
        }
        this.board = newBoard;
        this.board.print();
        System.out.println();
    }

    private int countNewState(Cell cell) {
        int numberOfAliveNeighbours = cell.countNumberOfAliveNeighbours();

//        Komórka ożywa, gdy ma dokładnie trzech sąsiadów żywych;
        if (!cell.isAlive()) {
            if (numberOfAliveNeighbours == 3) return 1;
            else return 0;
        } else {
//            Komórka przeżywa, gdy ma dwóch lub trzech sąsiadów żywych
            if ((numberOfAliveNeighbours == 2 || numberOfAliveNeighbours == 3)) return 1;
            else return 0;
        }
    }
}
