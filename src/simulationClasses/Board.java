package simulationClasses;

import sim.random.SimGenerator;
import java.util.Optional;

public class Board {
    private final int rows;
    private final int columns;
    private Cell[][] cells;
    private SimGenerator simGenerator = new SimGenerator();

    public Board(int rows, int columns) {
        this.rows = rows;
        this.columns = columns;
        this.cells = new Cell[this.rows][this.columns];
        completeCellsWithZeros();
    }

    public void copyCellValues(Board fromBoard) {
        for (int i = 0; i < rows; i++) {
            for (int j = 0; j < columns; j++) {
                int state = fromBoard.getCells()[i][j].getState();
                this.cells[i][j] = new Cell(this, i, j, state);
            }
        }
    }

    private void completeCellsWithZeros() {
        for (int i = 0; i < rows; i++) {
            for (int j = 0; j < columns; j++) {
                this.cells[i][j] = new Cell(this, i,j,0);
            }
        }
    }

    public void fillInRandomly() {
        for (int i = 0; i < rows; i++) {
            for (int j = 0; j < columns; j++) {
                int state = simGenerator.uniformInt(2);
                this.cells[i][j] = new Cell(this, i,j,state);
            }
        }
    }

    public Cell[][] getCells() {
        return cells;
    }

    public Optional<Cell> getCell(int x, int y) {
        if (x < 0 || x > rows || y < 0 || y > columns)
            return Optional.empty();
        else
            return Optional.of(cells[x][y]);
    }

    public void setCell(int x , int y, int newValue) {
        this.cells[x][y] = new Cell(this, x,y,newValue);
    }

    public void print() {
        for (int i = 0; i < rows; i++) {
            for (int j = 0; j < columns; j++) {
                System.out.print(cells[i][j].getState() + " ");
            }
            System.out.println();
        }
    }

    public int getRows() {
        return rows;
    }

    public int getColumns() {
        return columns;
    }
}
