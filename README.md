# Cellular Automaton

##  Opis projektu
Projekt zawiera implementację "Gry w życie" Conwaya według zasad:

- Komórka może być “żywa” lub “martwa”;
- Komórka ożywa, gdy ma dokładnie trzech sąsiadów żywych;
- Komórka przeżywa, gdy ma dwóch lub trzech sąsiadów żywych (umiera, gdy ma ich mniej niż dwoje - z samotności, lub gdy ma ich więcej niż czworo - z przeludnienia);

Należy przedstawić wynik automatu po N generacjach  (20<N<50)

[Artykuł z opisem problemu](https://natureofcode.com/book/chapter-7-cellular-automata/)